package ru.remezov.unique;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        Map<String, String> people = new HashMap<>();

//        people.put("Вася", "Иванов");
//        people.put("Петя", "Петров");
//        people.put("Виктор", "Сидоров");
//        people.put("Сергей", "Савельев");
//        people.put("Вадим", "Викторов");

//        people.put("Вася", "Иванов");
//        people.put("Петя", "Петров");
//        people.put("Виктор", "Иванов");
//        people.put("Сергей", "Савельев");
//        people.put("Вадим", "Петров");

        System.out.println(isUnique2(people));
    }

    //O(N^2)
    public static boolean isUnique(Map<String, String> map) {
        for (Map.Entry<String, String> item1: map.entrySet()) {
            for (Map.Entry<String, String> item2: map.entrySet()) {
                if ((item1.getValue().equals(item2.getValue())) && (item1 != item2)) {
                    return false;
                }
            }
        }
        return true;
    }

    //O(N)
    public static boolean isUnique2(Map<String, String> map) {
        Set<String> keys = new HashSet<>();
        keys.addAll(map.keySet());
        String value;
        for (String key: keys) {
            value = map.get(key);
            map.remove(key);
            if (map.containsValue(value) == true) {
                return false;
            }
        }
        return true;
    }
}
